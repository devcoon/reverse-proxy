#!/bin/env bash
# $Id: entrypoint.sh,v 0.1 2016/04/10 $ $Author: Pietro Bonaccorso $

docker run --rm \
-p "80:80" \
-v /var/run/docker.sock:/var/run/docker.sock \
-v $(pwd)/:/docker \
--name rp \
mosaicoon/reverse-proxy:latest

#-v /var/run/docker.sock:/var/run/docker.sock \
#-v $(pwd):/docker \
#-v $(pwd)/scripts:/docker/scripts \