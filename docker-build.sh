#!/bin/env bash
# $Id: docker-build.sh,v 0.1 2016/04/10 $ $Author: Pietro Bonaccorso $

docker build \
-t mosaicoon/reverse-proxy:latest \
./

#--no-cache \