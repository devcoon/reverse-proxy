FROM nginx:1.9-alpine

MAINTAINER Pietro Bonaccorso "pietro.b@mosaicoon.com"
LABEL description="Reverse proxy using Nginx 1.9 in alpine dist"

ENV REFRESH_TIME 2
ENV SERVERNAME_KEY "SERVER_NAME"
ENV SERVERPORT_KEY "SERVER_PORT"
ENV VERBOSE_NGINX false

#install bash
RUN apk --update add bash

RUN apk add --update curl bash \
  && rm -rf /var/cache/apk/* \
  && curl -L https://get.docker.com/builds/Linux/x86_64/docker-latest > /usr/bin/docker \
  && chmod +x /usr/bin/docker \
  && apk del curl

RUN apk --update add php php-json

#Add configuration files
COPY . /docker

#remove apk cache
RUN rm -rf /var/cache/apk/*

#expose ports
EXPOSE 80 443

ENTRYPOINT ["bash", "/docker/scripts/entrypoint.sh"]
CMD ["start-proxy"]

WORKDIR $APP_CWD