#!/bin/env bash
# $Id: bootloader.sh,v 0.1 2016/04/10 $ $Author: Pietro Bonaccorso $

#add vhost configuration template
cp /docker/configuration/nginx/default.conf /etc/nginx/conf.d/default.conf


for containerId in $(docker ps -aq); do

    #docker inspect -f "{{ index (index .Config.Env) 1 }}" $containerId

done


for var in $(printenv); do

    #explode vars to retrive key/value pairs
    IFS='=' read -r -a array <<< $var

    export KEY=${array[0]}

    if [[ $KEY =~ VHOST_|PHP_|FPM_ ]]; then

        export VALUE=${array[1]}

        sed -i -e 's|<'$KEY'>|'$VALUE'|g' '/etc/nginx/conf.d/default.conf'

    fi

done