#!/bin/env bash
# $Id: dynamic-reverse-proxy.sh,v 0.1 2016/04/10 $ $Author: Pietro Bonaccorso $

#start nginx main process
nginx -g "daemon off;" > /dev/null 2>&1 &

#start controller
php /docker/scripts/controller.php
