#!/bin/env bash
# $Id: entrypoint.sh,v 0.1 2016/04/10 $ $Author: Pietro Bonaccorso $

if [ "$1" = "start-proxy" ]; then


    bash /docker/scripts/dynamic-reverse-proxy.sh

    #echo ok

    # while :
    # do
    #     echo "Press [CTRL+C] to stop.."
    #     sleep 5
    # done

    #call supervisord to launch the proxy
    #/usr/bin/supervisord --nodaemon --configuration=/docker/configuration/supervisord/supervisor.conf

fi
