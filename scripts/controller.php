<?php

/**
 * @return void
 */
function reloadNginx()
{
    shell_exec("nginx -s reload");
}

/**
 * @param array $servers
 * @return bool
 */
function loadConfiguration(array $servers)
{
    var_dump($servers);
}

/**
 * exec docker command
 *
 * @param string $cmd
 * @return string
 */
function dockerCommand($cmd)
{
    return shell_exec("docker $cmd");
}

/**
 * @return array
 */
function getContainers()
{
    return array_filter(explode(PHP_EOL, dockerCommand('ps -aq')), function($var) {
        return ! empty($var);
    });
}

/**
 * @param string $containerId
 * @return array
 */
function inspectContainer($containerId)
{
    return json_decode(dockerCommand("inspect $containerId"), true);
}


/**
 * @param string $containerId
 * @param string $key (optional)
 * @return mixed
 */
function getEnvVars($containerId, $key = null)
{
    $containerInfo = inspectContainer($containerId);

    //get Env vars...
    $envs = [];
    foreach ($containerInfo[0]['Config']['Env'] as $value) {
        $res = explode('=', $value);
        $envs[$res[0]] = $res[1];
    }

    $res = ! empty($key) ? (! empty($envs[$key]) ? $envs[$key] : null) : $envs;

    return $res;
}

/**
 * @return void
 */
function llog($message)
{
    $date = date('Y-m-d H:i:s');
    echo("[{$date}] - {$message}");
}

/**
 * @return int
 */
function main()
{
    $refreshTime = getenv('REFRESH_TIME');

    $serverNameKey = getenv('SERVERNAME_KEY');
    $serverPortKey = getenv('SERVERPORT_KEY');

    $nginxPID = null;

    $containers = [];


    do {

        $currContainers = getContainers();

        $servers = [];

        if($currContainers !== $containers) {

            llog('Container activity changed');

            $containers = $currContainers;
            foreach ($containers as $containerId) {

                $serverName = getEnvVars($containerId, $serverNameKey);
                $serverPort = getEnvVars($containerId, $serverPortKey);

                if(! empty($serverName) && ! empty($serverPort)) {
                    $servers[] = [
                        'servername' => $serverName,
                        'port' => $serverPort
                    ];
                }

            }
        }

        //restart nginx with new configuration
        if(! empty($servers)) {

            llog('Changing Reverse Proxy Configuration');

            //load configuration
            loadConfiguration($servers);

            llog('Reloading Nginx');
            reloadNginx();

            //print /etc/host row
            //...
        }

        sleep($refreshTime);

    } while (true);
}


// --------------------------------------------------------------------
// MAIN
// --------------------------------------------------------------------

main();

